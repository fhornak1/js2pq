pub trait DisplayEnumType {
    fn display(&self) -> &'static str;
}
