
use serde_json::{Value,Map};

pub type JSObj = Map<String, Value>;
pub type JSArray = Vec<Value>;

pub trait Get<T> {
    fn get_value(&self, name: &String) -> Result<T, String> {
        match self.get_value_opt(name) {
            Some(v) => Ok(v),
            None => Err(format!("Value for `{}` not found!", name)),
        }
    }

    fn get_value_opt(&self, name: &String) -> Option<T>;
}

pub fn tree_get<'a>(obj: &'a JSObj, path: Vec<String>) -> Option<&'a JSObj> {
    let mut stack: Vec<&'a JSObj> = vec![obj];

    for pth_node in path.iter() {
        let node = stack.pop()?;

        match node.get(pth_node)? {
            Value::Object(obj) => stack.push(obj),
            _ => return None,
        }
    }

    stack.pop()
}

pub fn split_path(value: &String) -> Vec<String> {
    value.split('.').map(|s| s.to_owned()).collect()
}

pub fn split_path_str(value: &str) -> Vec<String> {
    value.split('.').map(|s| s.to_owned()).collect()
}

pub fn tget<'a>(obj: &'a JSObj, path: &[String]) -> Option<&'a JSObj> {
    let mut stack = [Some(obj)];

    for node_name in path {
        let node = stack[0]?;

        match node.get(node_name)? {
            Value::Object(next_node) => stack[0] = Some(next_node),
            _ => return None
        }
    }

    stack[0]
}

impl Get<u64> for JSObj {
    fn get_value_opt(&self, name: &String) -> Option<u64> {
        let val = self.get(name);
        if val.is_none() {
            return None;
        }
        match val.unwrap() {
            Value::Number(ref num) => num.as_u64(),
            _ => None,
        }
    }
}

impl Get<i64> for JSObj {
    fn get_value_opt(&self, name: &String) -> Option<i64> {
        if let Some(val) = self.get(name) {
            match val {
                Value::Number(ref num) => num.as_i64(),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl Get<f64> for JSObj {
    fn get_value_opt(&self, name: &String) -> Option<f64> {
        let val = self.get(name);
        if val.is_none() {
            return None;
        }
        match val.unwrap() {
            Value::Number(ref num) => num.as_f64(),
            _ => None,
        }
    }
}

impl Get<String> for JSObj {
    fn get_value_opt(&self, name: &String) -> Option<String> {
        let val = self.get(name);
        if val.is_none() {
            return None;
        }
        match val.unwrap() {
            Value::String(ref str_val) => Some(str_val.clone()),
            _ => None,
        }
    }
}

impl Get<bool> for JSObj {
    fn get_value_opt(&self, name: &String) -> Option<bool> {
        let val = self.get(name);
        if val.is_none() {
            return None;
        }
        match val.unwrap() {
            Value::Bool(ref bool_val) => Some(bool_val.clone()),
            _ => None,
        }
    }
}

impl Get<JSObj> for JSObj {
    fn get_value_opt(&self, name: &String) -> Option<JSObj> {
        if let Some(value) = self.get(name) {
            match value {
                Value::Object(obj) => Some(obj.clone()),
                _ => None,
            }
        } else {
            None
        }
    }
}

pub trait Pop<T: Sized + 'static + Clone> {
    fn pop(&mut self, node: &String) -> Option<T>;
}

impl Pop<u64> for JSObj {
    fn pop(&mut self, name: &String) -> Option<u64> {
        if let Some(value) = self.remove(name) {
            match value {
                Value::Number(num) => num.as_u64(),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl Pop<f64> for JSObj {
    fn pop(&mut self, name: &String) -> Option<f64> {
        if let Some(value) = self.remove(name) {
            match value {
                Value::Number(num) => num.as_f64(),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl Pop<JSObj> for JSObj {
    fn pop(&mut self, name: &String) -> Option<JSObj> {
        if let Some(value) = self.remove(name) {
            match value {
                Value::Object(map) => Some(map),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl Pop<bool> for JSObj {
    fn pop(&mut self, name: &String) -> Option<bool> {
        if let Some(value) = self.remove(name) {
            match value {
                Value::Bool(b) => Some(b),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl Pop<String> for JSObj {
    fn pop(&mut self, name: &String) -> Option<String> {
        if let Some(value) = self.remove(name) {
            match value {
                Value::String(string) => Some(string),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl Pop<i64> for JSObj {
    fn pop(&mut self, name: &String) -> Option<i64> {
        if let Some(value) = self.remove(name) {
            match value {
                Value::Number(num) => num.as_i64(),
                _ => None,
            }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    fn data() -> JSObj {
        let data = r#"
            {
                "foo": {
                    "bar": 1,
                    "baz": {
                        "qux": {
                            "quux": 1
                        },
                        "quux": {
                            "bar": [1, 2, 3, 4]
                        }
                    },
                    "qux": {
                        "foo": 2,
                        "bar": [
                            {
                                "a": 1,
                                "b": {
                                    "c": 2,
                                    "d": "asdf"
                                }
                            },
                            {
                                "a": 2,
                                "c": [1, 2, 3, 4]
                            }
                        ]
                    }
                },
                "bar": 1,
                "baz": -1,
                "qux": -1.5,
                "quux": "Some string",
                "quuux": {
                    "a": 1,
                    "b": "asdf",
                    "c": true
                },
                "quuuux": true
            }"#;
        match serde_json::from_str(&data).unwrap() {
            serde_json::Value::Object(d) => d,
            _ => panic!("Unable to deserialize json object `{:?}`", data)
        }
    }

    fn create_js_obj(value: &'static str) -> JSObj {
        match serde_json::from_str::<serde_json::Value>(&value).unwrap() {
            serde_json::Value::Object(o) => o,
            _ => panic!("Unable to deserialize json object `{:?}`", value)
        }
    }

    #[test]
    fn test_tget() {
        assert_eq!(Some(&create_js_obj(r#"{"quux": 1}"#)), tget(&data(), &["foo".to_owned(), "baz".to_owned(), "qux".to_owned()]));
    }

    // #[test]
    // fn test_tget_missing() {
    //     assert_eq!(None, tget(&data(), &["a".to_owned(), "b".to_owned()]);
    // }

    #[test]
    fn test_Get_uint() {
        assert_eq!(Some(1u64), data().get_value_opt(&"bar".to_owned()))
    }

    #[test]
    fn test_Get_int() {
        assert_eq!(Some(-1i64), data().get_value_opt(&"baz".to_owned()))
    }

    #[test]
    fn test_Get_f64() {
        assert_eq!(Some(-1.5f64), data().get_value_opt(&"qux".to_owned()))
    }

    #[test]
    fn test_Get_bool() {
        assert_eq!(Some(true), data().get_value_opt(&"quuuux".to_owned()))
    }

    #[test]
    fn test_Get_string() {
        assert_eq!(Some("Some string".to_owned()), data().get_value_opt(&"quux".to_owned()));
        assert_eq!(None, Get::<String>::get_value_opt(&data(), &"qux".to_owned()));
    }

    #[test]
    fn test_Get_obj() {
        assert_eq!(Some(create_js_obj(r#"{"a": 1, "b": "asdf", "c": true}"#)), data().get_value_opt(&"quuux".to_owned()))
    }

    #[test]
    fn test_split_path() {
        assert_eq!(vec!["foo".to_owned(), "bar".to_owned()], split_path_str(&"foo.bar"));
        assert_eq!(vec!["foo".to_owned(), "bar".to_owned()], split_path(&"foo.bar".to_owned()));
    }
}
