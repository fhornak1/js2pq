use crate::schema::{JsonField, Schema, Table, Push, Type};
use crate::json_helpers::{JSObj, Pop, Get, tree_get};
use serde_json::{Value,Number};
use std::iter;
use std::fmt;
use std::convert;

pub struct JsonReader<'a> {
    stack_depth: usize,
    paths: Vec<&'a JsonField>,
}

impl<'a> JsonReader<'a> {
    pub fn new(schema: &'a Schema) -> Self {
        let mut reader = Self {
            stack_depth: 0,
            paths: Vec::with_capacity(schema.len()),
        };

        reader.paths = schema.iter().collect();

        reader.paths.sort_by_key(|f| f.path());

        reader.stack_depth = reader.paths.len();

        reader
    }

    pub fn push<'b>(&self, table: &'b mut Table, obj: JSObj) -> &'b mut Table {
        for path in self.paths.iter() {
            let path_nodes = path.path().as_vec();
            let leaf: &String = &path_nodes[path_nodes.len() - 1];

            let leaf_object = tree_get(&obj, path_nodes[..path_nodes.len() - 1].to_vec()).unwrap();

            match path.field_type() {
                Type::Bool => {
                    let val: Option<bool> = leaf_object.get_value_opt(leaf);
                    table.push_value(path.name(), val).unwrap();
                }
                Type::Float => {
                    let val: Option<f64> = leaf_object.get_value_opt(leaf);
                    table.push_value(path.name(), val).unwrap();
                }
                Type::Int => {
                    let val: Option<i64> = leaf_object.get_value_opt(leaf);
                    table.push_value(path.name(), val).unwrap();
                }
                Type::UInt => {
                    let val: Option<u64> = leaf_object.get_value_opt(leaf);
                    table.push_value(path.name(), val).unwrap();
                }
                Type::Str => {
                    let val: Option<String> = leaf_object.get_value_opt(leaf);
                    table.push_value(path.name(), val).unwrap();
                }
                _ => panic!("Unsupported type {:?}", path.field_type()),
            }
        }
        table
    }
}

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone,Hash)]
pub enum Index {
    ListIndex(usize),
    ObjKey(String)
}

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone,Hash)]
pub struct JsonNestedIndex {
    path: Vec<Index>
}

impl JsonNestedIndex {
    pub fn new() -> Self {
        JsonNestedIndex {
            path: vec![]
        }
    }

    pub fn cat(root: &[Index], node: Index) -> Self {
        let mut index = JsonNestedIndex {
            path: Vec::with_capacity(root.len() + 1)
        };
        index.path = root.iter().map(|i| i.clone()).collect();
        index.path[root.len()] = node;
        
        index
    }

    pub fn push(&self, node: Index) -> Self {
        let mut root_idx = self.clone();
        root_idx.path.push(node);
        root_idx
    }

    pub fn push_pos(&self, index: usize) -> Self {
        self.push(Index::ListIndex(index))
    }

    pub fn push_key(&self, key: String) -> Self {
        self.push(Index::ObjKey(key))
    }

    pub fn push_key_str(&self, key: &str) -> Self {
        self.push_key(key.to_owned())
    }

    pub fn len(&self) -> usize {
        self.path.len()
    }

    pub fn iter(&self) -> &[Index] {
        &self.path[..]
    }
}

impl convert::TryFrom<String> for JsonNestedIndex {
    type Error = String;
    
    fn try_from(value: String) -> Result<Self, Self::Error> {
        let path: Result<Vec<Index>, String> = value.split('.').map(|s| {
            if s.len() > 1 && s.chars().next().unwrap() == '@' {
                match usize::from_str_radix(&s[1..], 10) {
                    Ok(list_idx) => Ok(Index::ListIndex(list_idx)),
                    Err(_) => Err(format!("Parsing of list position `{}` faild, is it written in `@<radix 10 number>` format?", s))
                }
            } else {
                Ok(Index::ObjKey(s.to_owned()))
            }
        }).collect();
        Ok(JsonNestedIndex {path: path?})
    }
}

impl fmt::Display for JsonNestedIndex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.path.iter().map(|i| match i {
            Index::ListIndex(n) => format!("@{}", n),
            Index::ObjKey(k) => k.to_owned()
        }).collect::<Vec<String>>().join("."))
    }
}

#[derive(PartialEq,PartialOrd,Debug)]
pub enum PrimitiveValue {
    Int(i64),
    UInt(u64),
    Float(f64),
    Str(String),
    Bool(bool),
    Null
}

impl fmt::Display for PrimitiveValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PrimitiveValue::Int(i) => write!(f, "{}", i),
            PrimitiveValue::UInt(u) => write!(f, "{}", u),
            PrimitiveValue::Float(n) => write!(f, "{}", n),
            PrimitiveValue::Str(s) => write!(f, "{}", s),
            PrimitiveValue::Bool(b) => write!(f, "{}", b),
            PrimitiveValue::Null => write!(f, "null"),
        }
    }
}

pub struct FlatValueEntry {
    value: PrimitiveValue,
    root: JsonNestedIndex
}

impl FlatValueEntry {
    pub fn new_bool(b: bool, root: JsonNestedIndex) -> Self {
        FlatValueEntry {
            value: PrimitiveValue::Bool(b),
            root
        }
    } 

    pub fn new_int(i: i64, root: JsonNestedIndex) -> Self {
        FlatValueEntry {
            value: PrimitiveValue::Int(i),
            root
        }
    } 

    pub fn new_uint(u: u64, root: JsonNestedIndex) -> Self {
        FlatValueEntry {
            value: PrimitiveValue::UInt(u),
            root
        }
    } 

    pub fn new_float(f: f64, root: JsonNestedIndex) -> Self {
        FlatValueEntry {
            value: PrimitiveValue::Float(f),
            root
        }
    }

    pub fn new_str(s: String, root: JsonNestedIndex) -> Self {
        FlatValueEntry {
            value: PrimitiveValue::Str(s),
            root
        }
    } 

    pub fn new_null(root: JsonNestedIndex) -> Self {
        FlatValueEntry {
            value: PrimitiveValue::Null,
            root
        }
    }

    pub fn new_num(num: &Number, root: JsonNestedIndex) -> Self {
        if num.is_u64() {
            Self::new_uint(num.as_u64().unwrap(), root)
        } else if num.is_i64() {
            Self::new_int(num.as_i64().unwrap(), root)
        } else {
            Self::new_float(num.as_f64().unwrap(), root)
        }
    }
}

impl fmt::Display for FlatValueEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}: {}", self.root, self.value)
    }
}


pub struct FlatMapIter<'a> {
    stack: Vec<(JsonNestedIndex, &'a Value)>,
}

impl <'a> FlatMapIter<'a> {
    pub fn new(value: &'a Value) -> Self {
        Self::with_capacity(value, 100, JsonNestedIndex::new())
    }

    pub fn with_capacity(value: &'a Value, capacity: usize, root: JsonNestedIndex) -> Self {
        let mut iter = FlatMapIter {
            stack: Vec::with_capacity(std::cmp::min(capacity, 1))
        };

        iter.stack.push((root, value));

        iter
    }
}

impl <'a>iter::Iterator for FlatMapIter<'a> {
    type Item = FlatValueEntry;
    
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let (root, v) = self.stack.pop()?;
            match v {
                Value::Array(vec) => {
                    for (idx, value) in (&vec).iter().enumerate() {
                        self.stack.push((root.push_pos(idx), value))
                    }
                },
                Value::Object(obj) => {
                    for (key, value) in (&obj).iter() {
                        self.stack.push((root.push_key(key.clone()), value))
                    }
                },
                Value::Number(num) => return Some(FlatValueEntry::new_num(num, root)),
                Value::String(s) => return Some(FlatValueEntry::new_str(s.clone(), root)),
                Value::Bool(b) => return Some(FlatValueEntry::new_bool(b.clone(), root)),
                Value::Null => return Some(FlatValueEntry::new_null(root))
            }
        }
    }
}


pub fn walk<'a>(value: &'a Value, nested_index: &JsonNestedIndex) -> Option<&'a Value> {
    let mut stack: [&Value; 1] = [value];
    for idx in nested_index.iter() {
        match (idx, stack[0]) {
            (Index::ListIndex(pos), Value::Array(vec)) => {
                stack[0] = match vec.get(*pos) {
                    Some(val) => val,
                    None => return None
                };
            },
            (Index::ObjKey(key), Value::Object(js_obj)) => {
                stack[0] = match js_obj.get(key) {
                    Some(val) => val,
                    None => return None
                }
            },
            _ => return None
        }
    }
    Some(stack[0])
}

pub trait Depth {
    fn depth(&self) -> usize;
}

impl Depth for String {
    fn depth(&self) -> usize {
        self.chars().filter(|ch| *ch == '.').count()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;
    use std::collections::HashMap;

    #[test]
    fn test_JsonReader_new_ordering() {
        let mut s = Schema::new();
        s.push(JsonField::new("foo".to_owned()));
        s.push(JsonField::new("bar.baz".to_owned()));
        s.push(JsonField::new("bar.quux.foo".to_owned()));
        s.push(JsonField::new("bar.quux.qux".to_owned()));
        s.push(JsonField::new("baz.foo.bar".to_owned()));
        s.push(JsonField::new("qux.quux.bar".to_owned()));
        s.push(JsonField::new("quux".to_owned()));

        let r = JsonReader::new(&s);

        assert_eq!(
            vec![
                "foo".to_owned(),
                "quux".to_owned(),
                "bar.baz".to_owned(),
                "bar.quux.foo".to_owned(),
                "bar.quux.qux".to_owned(),
                "baz.foo.bar".to_owned(),
                "qux.quux.bar".to_owned()
            ],
            r.paths
                .iter()
                .map(|ref f| f.path().as_string())
                .collect::<Vec<String>>()
        )
    }

    #[test]
    fn test_JsonNestedIndex_push() {
        let mut idx = JsonNestedIndex::new().push_key_str("foo").push_key_str("bar").push_pos(12).push_key_str("baz");
        let expected = vec![
            Index::ObjKey("foo".to_owned()),
            Index::ObjKey("bar".to_owned()),
            Index::ListIndex(12),
            Index::ObjKey("baz".to_owned()),
        ];
        assert_eq!(expected, idx.path);
    }

    #[test]
    fn test_walk() {
        let json_data = r#"
            {
                "a": {
                    "b": [
                        {"c": "d", "e": 1},
                        {"f": 2, "g": "h"}
                    ],
                    "i": true
                },
                "j": null
            }"#;
        let data = serde_json::from_str(&json_data).unwrap();
        let idx = JsonNestedIndex::new().push_key_str("a").push_key_str("b").push_pos(1).push_key_str("g");

        assert_eq!(Some(&Value::String("h".to_owned())), walk(&data, &idx));
        assert_eq!(Some(&Value::Null), walk(&data, &JsonNestedIndex::new().push_key_str("j")))
    }

    #[test]
    fn test_JsonNestedIndex_try_from() {
        use std::convert::TryInto;
        let value = "foo.bar.@1.baz.@..@34".to_owned();
        let expected = Ok(JsonNestedIndex{
            path: vec![
                Index::ObjKey("foo".to_owned()),
                Index::ObjKey("bar".to_owned()),
                Index::ListIndex(1),
                Index::ObjKey("baz".to_owned()),
                Index::ObjKey("@".to_owned()),
                Index::ObjKey("".to_owned()),
                Index::ListIndex(34)
            ],
        });
        assert_eq!(expected, value.try_into())
    }

    #[test]
    fn test_tree_flatten() {
        use std::convert::TryInto;
        let json_data = r#"
            {
                "a": {
                    "b": [
                        {"c": "d", "e": 1},
                        {"f": 2, "g": "h"}
                    ],
                    "i": true
                },
                "j": null
            }"#;
        let data = serde_json::from_str(&json_data).unwrap();
        let flat_data = FlatMapIter::new(&data).map(|e| {(e.root, e.value)}).collect::<HashMap<JsonNestedIndex, PrimitiveValue>>();
        let expected: HashMap<JsonNestedIndex, PrimitiveValue> = vec![
            ("a.b.@0.c".to_owned().try_into().unwrap(), PrimitiveValue::Str("d".to_owned())),
            ("a.b.@0.e".to_owned().try_into().unwrap(), PrimitiveValue::UInt(1)),
            ("a.b.@1.f".to_owned().try_into().unwrap(), PrimitiveValue::UInt(2)),
            ("a.b.@1.g".to_owned().try_into().unwrap(), PrimitiveValue::Str("h".to_owned())),
            ("a.i".to_owned().try_into().unwrap(), PrimitiveValue::Bool(true)),
            ("j".to_owned().try_into().unwrap(), PrimitiveValue::Null),
        ].into_iter().collect::<HashMap<JsonNestedIndex,PrimitiveValue>>();
        assert_eq!(expected, flat_data)
    }
}
