#[macro_use]
extern crate serde;

extern crate named_type;
extern crate named_type_derive;
extern crate serde_json;
extern crate arrow;
extern crate parquet;

use serde_json::{Result, Value};

mod json_reader;
mod picker;
mod schema;
mod utils;
mod json_helpers;
mod mapping;
mod json_schema;
mod data_struct;

pub const PATH_SEPARATOR: char = '.';
// {
//   "name1": {
//     "mandatory": true,
//     "type": any,
//   },
//   "name2": {
//     "type": "object",
//     "mandatory": true
//   },
//   "name2.prop1": {
//     "type": "int",
//     "mandatory": true
//   },
//   "name2.prop2": {
//     "type": "list[int]",
//     "mandatory": true,
//     "elements": "int"
//   }
//   "_others": {
//     "type": object[any],
//     "mandatory": true
//   }
// }
//
//   type: [any, bool,int, float, str, list, object]

fn main() {
    println!("Hello, world!");
    crate::mapping::test_parquet();
}
