
use serde::{Serialize,Deserialize};
use parquet::basic;
use parquet::data_type;
use crate::schema::JsonField;

const DELIM: char = '.';

#[derive(Debug,PartialEq,Eq,Clone,Serialize,Deserialize)]
#[serde(tag = "__type__")]
pub enum JsonType{
    Bool { optional: bool, path: Vec<String>, maps_to: String },
    Int { optional: bool, variant: IntVariant, path: Vec<String>, maps_to: String },
    Float { optional: bool, variant: FloatVariant, path: Vec<String>, maps_to: String },
    Str { optional: bool, fixed_length: Option<usize>, path: Vec<String>, maps_to: String },
    List { optional: bool, of_type: Box<JsonType>, path: Vec<String>, maps_to: String },
    Obj { optional: bool, of_type: Box<JsonType>, path: Vec<String>, maps_to: String },
}

pub struct Mapping {
    name: String,
    // type: Parquet::Type
}

#[derive(Debug,PartialEq,Eq,Copy,Clone,Serialize,Deserialize)]
#[serde(tag = "__type__")]
pub enum IntVariant {
    I32,
    I64,
    I96,
}

#[derive(Debug,PartialEq,Eq,Copy,Clone,Serialize,Deserialize)]
#[serde(tag = "__type__")]
pub enum FloatVariant {
    F32,
    F64
}

pub trait JsonTypeBuilder {
    type S: JsonTypeBuilder + 'static + Sized;

    fn from_str(path: &str) -> Self::S {
        Self::new(path.split(DELIM).map(|s| s.to_owned()).collect())
    }

    fn from_string(path: &String) -> Self::S {
        Self::from_str(path.as_ref())
    }

    fn from_slice(path: &[String]) -> Self::S{
        Self::new(path.iter().map(|s| s.clone()).collect())
    }

    fn new(path: Vec<String>) -> Self::S;

    fn new_child() -> Self::S {
        Self::new(vec![])
    }

    fn optional(&mut self, optional: bool) -> &mut Self;

    fn maps_to(&mut self, name: String) -> &mut Self;

    fn build(&self) -> JsonType;
}

pub trait BoolBuilder: JsonTypeBuilder {
    type S: BoolBuilder + 'static + Sized;
}

pub trait IntBuilder: JsonTypeBuilder {
    type S: IntBuilder + 'static + Sized;
    fn variant(&mut self, variant: IntVariant) -> &mut Self;
}

pub trait FloatBuilder: JsonTypeBuilder {
    type S: FloatBuilder + 'static + Sized;
    fn variant(&mut self, variant: FloatVariant) -> &mut Self;
}

pub trait StrBuilder: JsonTypeBuilder {
    type S: StrBuilder + 'static + Sized;

    fn fixed_size(&mut self, size: Option<usize>) -> &mut Self;
}

pub trait NestedBuilder: JsonTypeBuilder {
    type S: NestedBuilder + 'static + Sized;

    fn of_type(&mut self, json_type: JsonType) -> &mut Self;
}

pub struct BoolBuilderHandle {
    path: Vec<String>,
    optional: bool,
    maps_to: String
}

impl JsonTypeBuilder for BoolBuilderHandle {
    type S = Self;

    fn new(path: Vec<String>) -> Self {
        BoolBuilderHandle {
            optional: false,
            maps_to: defautl_maps_to(&path[..]),
            path
        }
    }

    fn maps_to(&mut self, name: String) -> &mut Self {
        self.maps_to = name;
        self
    }

    fn optional(&mut self, optional: bool) -> &mut Self {
        self.optional = optional;
        self
    }

    fn build(&self) -> JsonType {
        JsonType::Bool {
            optional: self.optional,
            path: self.path.to_owned(),
            maps_to: self.maps_to.to_owned()
        }
    }
}

impl BoolBuilder for BoolBuilderHandle {
    type S = BoolBuilderHandle;
}

pub struct IntBuilderHandle {
    path: Vec<String>,
    optional: bool,
    variant: IntVariant,
    maps_to: String,
}

impl JsonTypeBuilder for IntBuilderHandle {
    type S = Self;

    fn new(path: Vec<String>) -> Self {
        IntBuilderHandle {
            optional: false,
            variant: IntVariant::I64,
            maps_to: defautl_maps_to(&path[..]),
            path
        }
    }

    fn optional(&mut self, optional: bool) -> &mut Self {
        self.optional = optional;
        self
    }

    fn maps_to(&mut self, name: String) -> &mut Self {
        self.maps_to = name;
        self
    }

    fn build(&self) -> JsonType {
        JsonType::Int {
            optional: self.optional,
            variant: self.variant,
            path: self.path.to_owned(),
            maps_to: self.maps_to.to_owned()
        }
    }
}

impl IntBuilder for IntBuilderHandle {
    type S = IntBuilderHandle;

    fn variant(&mut self, variant: IntVariant) -> &mut Self {
        self.variant = variant;
        self
    }
}

pub struct FloatBuilderHandle {
    path: Vec<String>,
    optional: bool,
    variant: FloatVariant,
    maps_to: String,
}

impl JsonTypeBuilder for FloatBuilderHandle {
    type S = Self;

    fn new(path: Vec<String>) -> Self {
        FloatBuilderHandle {
            optional: false,
            variant: FloatVariant::F64,
            maps_to: defautl_maps_to(&path[..]),
            path
        }
    }

    fn optional(&mut self, optional: bool) -> &mut Self {
        self.optional = optional;
        self
    }

    fn maps_to(&mut self, name: String) -> &mut Self {
        self.maps_to = name;
        self
    }

    fn build(&self) -> JsonType {
        JsonType::Float {
            optional: self.optional,
            variant: self.variant,
            maps_to: self.maps_to.to_owned(),
            path: self.path.to_owned()
        }
    }
}

impl FloatBuilder for FloatBuilderHandle {
    type S = FloatBuilderHandle;

    fn variant(&mut self, variant: FloatVariant) -> &mut Self {
        self.variant = variant;
        self
    }
}

pub struct StrBuilderHandle {
    path: Vec<String>,
    optional: bool,
    fixed_length: Option<usize>,
    maps_to: String,
}

impl StrBuilder for StrBuilderHandle {
    type S = StrBuilderHandle;

    fn fixed_size(&mut self, size: Option<usize>) -> &mut Self {
        self.fixed_length = size;
        self
    }
}

impl JsonTypeBuilder for StrBuilderHandle {
    type S = Self;

    fn new(path: Vec<String>) -> Self {
        StrBuilderHandle {
            optional: false,
            fixed_length: None,
            maps_to: defautl_maps_to(&path[..]),
            path
        }
    }

    fn optional(&mut self, optional: bool) -> &mut Self {
        self.optional = optional;
        self
    }

    fn maps_to(&mut self, name: String) -> &mut Self {
        self.maps_to = name;
        self
    }

    fn build(&self) -> JsonType {
        JsonType::Str {
            optional: self.optional,
            fixed_length: self.fixed_length,
            path: self.path.to_owned(),
            maps_to: self.maps_to.to_owned()
        }
    }
}

pub struct ListBuilderHandle {
    path: Vec<String>,
    optional: bool,
    of_type: Box<JsonType>,
    maps_to: String,
}

impl JsonTypeBuilder for ListBuilderHandle {
    type S = Self;

    fn new(path: Vec<String>) -> Self {
        ListBuilderHandle {
            optional: false,
            of_type: Box::new(StrBuilderHandle::new_child().build()),
            maps_to: defautl_maps_to(&path[..]),
            path
        }
    }

    fn optional(&mut self, optional: bool) -> &mut Self {
        self.optional = optional;
        self
    }

    fn maps_to(&mut self, name: String) -> &mut Self {
        self.maps_to = name;
        self
    }

    fn build(&self) -> JsonType {
        JsonType::List {
            optional: self.optional,
            of_type: self.of_type.clone(),
            path: self.path.to_owned(),
            maps_to: self.maps_to.to_owned()
        }
    }
}

impl NestedBuilder for ListBuilderHandle {
    type S = ListBuilderHandle;

    fn of_type(&mut self, json_type: JsonType) -> &mut Self {
        *self.of_type = json_type;
        self
    }
}

pub struct MapBuilderHandle {
    path: Vec<String>,
    of_type: Box<JsonType>,
    optional: bool,
    maps_to: String,
}

impl JsonTypeBuilder for MapBuilderHandle {
    type S = Self;

    fn new(path: Vec<String>) -> Self {
        MapBuilderHandle {
            optional: false,
            of_type: Box::new(StrBuilderHandle::new_child().build()),
            maps_to: defautl_maps_to(&path[..]),
            path,
        }
    }

    fn maps_to(&mut self, name: String) -> &mut Self {
        self.maps_to = name;
        self
    }

    fn optional(&mut self, optional: bool) -> &mut Self {
        self.optional = optional;
        self
    }

    fn build(&self) -> JsonType {
        JsonType::Obj {
            optional: self.optional,
            of_type: self.of_type.clone(),
            path: self.path.to_owned(),
            maps_to: self.maps_to.to_owned()
        }
    }
}

impl NestedBuilder for MapBuilderHandle {
    type S = MapBuilderHandle;

    fn of_type(&mut self, json_type: JsonType) -> &mut Self {
        *self.of_type = json_type;
        self
    }
}

pub fn defautl_maps_to(path: &[String]) -> String {
    path.join("_")
}

#[derive(Debug,PartialEq,Eq)]
pub struct JsonSchema {
    json_fields: Vec<JsonType>,
}

impl JsonSchema {
    pub fn new() -> Self {
        Self {
            json_fields: vec![]
        }
    }

    // TODO: add checks
    pub fn push(&mut self, field: JsonType) {
        self.json_fields.push(field);
    }

    pub fn fields(&self) -> &[JsonType] {
        &self.json_fields[..]
    }

    pub fn field(&self, idx: usize) -> Option<&JsonType> {
        self.json_fields.get(idx)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_builder_from_str() {
        let mut field_type= BoolBuilderHandle::from_str("foo.bar.baz")
            .optional(true)
            .build();
        assert_eq!(
            JsonType::Bool {
                path: vec!["foo".to_owned(), "bar".to_owned(), "baz".to_owned()],
                maps_to: "foo_bar_baz".to_owned(),
                optional: true
            },
            field_type
        );
    }

    #[test]
    fn test_build_from_slice() {
        let mut field_type = BoolBuilderHandle::from_slice(&vec!["foo".to_owned()]).build();
        assert_eq!(
            JsonType::Bool {
                path: vec!["foo".to_owned()],
                maps_to: "foo".to_owned(),
                optional: false
            },
            field_type
        );
    }

    #[test]
    fn test_build_from_string() {
        let mut field_type = BoolBuilderHandle::from_string(&"foo.bar.baz".to_owned()).build();
        assert_eq!(
            JsonType::Bool {
                path: vec!["foo".to_owned(), "bar".to_owned(), "baz".to_owned()],
                maps_to: "foo_bar_baz".to_owned(),
                optional: false
            },
            field_type
        );
    }

    #[test]
    fn test_build_map_type() {
        let field_type = MapBuilderHandle::from_str("foo.bar.baz")
            .optional(true)
            .of_type(
                ListBuilderHandle::new_child().of_type(
                    IntBuilderHandle::new_child().variant(IntVariant::I32).build()
                ).build()
            )
            .build();
        assert_eq!(
            JsonType::Obj {
                path: vec!["foo".to_owned(), "bar".to_owned(), "baz".to_owned()],
                optional: true,
                maps_to: "foo_bar_baz".to_owned(),
                of_type: Box::new(
                    JsonType::List {
                        path: vec![],
                        optional: false,
                        maps_to: "".to_owned(),
                        of_type: Box::new(
                            JsonType::Int {
                                optional: false,
                                path: vec![],
                                maps_to: "".to_owned(),
                                variant: IntVariant::I32
                            }
                        )
                    }
                )
            },
            field_type
        );
    }
}
