
use parquet::data_type::Int96;
use serde_json::{Map, Value};

pub enum Column {
    F64(Vec<f64>),
    U64(Vec<u64>),
    I64(Vec<i64>),
    F32(Vec<f32>),
    U32(Vec<u32>),
    I32(Vec<i32>),
    U96(Vec<Int96>),
    I96(Vec<Int96>),
    Bool(Vec<bool>),
    Str(Vec<String>),
    OptionF64(Vec<Option<f64>>),
    OptionU64(Vec<Option<u64>>),
    OptionI64(Vec<Option<i64>>),
    OptionF32(Vec<Option<f32>>),
    OptionU32(Vec<Option<u32>>),
    OptionI32(Vec<Option<i32>>),
    OptionU96(Vec<Option<Int96>>),
    OptionI96(Vec<Option<Int96>>),
    OptionBool(Vec<Option<bool>>),
    OptionStr(Vec<Option<String>>),
}

// list:
// [
//      [
//          [1, 2 ,3],
//          null
//      ],
//      [
//          null,
//          null,
//          [1],
//          [null]
//      ]
//  ]
// ->
// 1, 0
// 2, 2
// 3, 2
// null, 1
// null, 1
// null, 1
// 1, 2
// null, 2

pub struct FlatList<T: 'static + Sized + Clone> {
    rep_level: Vec<i16>,
    values: Vec<Option<T>>
}

pub struct FlatMap<T: 'static + Sized + Clone> {
    keys: Vec<String>,
    values: Vec<Option<T>>
}

// {
//      'a': null,
//      'b': [
//          'c',
//          'd',
//          'e'
//      ],
// }
// {
//      'a': ['b', 'c'],
//      'd': ['e', null, 'f']
//  }
//  ->
//  'a', null, 0
//  'b', 1
//  'c', 2
//  'd', 2
//  'e', 2
//  

impl <T> FlatMap<T>
where T: 'static + Sized + Clone
{
    pub fn new(map: Map<String, Value>) -> Self {
        FlatMap {
            keys: vec![],
            values: vec![]
        }
    }
}
