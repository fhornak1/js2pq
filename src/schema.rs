use named_type::NamedType;
use named_type_derive::*;
use serde_json::Value;
use std::cmp::{Ord, Ordering, PartialEq, PartialOrd};
use std::collections::HashMap;
use std::iter::IntoIterator;
use std::slice::Iter;
use crate::json_helpers::JSObj;

use parquet::data_type::Int96;

#[derive(Debug, PartialEq, Clone, Hash, NamedType)]
pub enum Type {
    Any,
    Int,
    UInt,
    Float,
    Bool,
    Str,
    List,
    Object,
}

#[derive(Debug, Clone, Hash, PartialEq, NamedType)]
pub struct JsonField {
    path: String,
    col_name: String,
    mandatory: bool,
    type_: Type,
}

impl JsonField {
    pub fn new(path: String) -> Self {
        let col_name = Self::name_from_path(&path);
        Self {
            path,
            col_name,
            mandatory: false,
            type_: Type::Any,
        }
    }

    pub fn set_name(&mut self, name: String) -> &mut Self {
        self.col_name = name;
        self
    }

    pub fn set_type_mut_ref(&mut self, type_: Type) -> &mut Self {
        self.type_ = type_;
        self
    }

    pub fn set_mandatory_mut_ref(&mut self, mandatory: bool) -> &mut Self {
        self.mandatory = mandatory;
        self
    }

    pub fn set_type(mut self, type_: Type) -> Self {
        self.type_ = type_;
        self
    }

    pub fn field_type(&self) -> &Type {
        &self.type_
    }

    pub fn name(&self) -> &String {
        &self.col_name
    }

    pub fn set_mandatory(mut self, mandatory: bool) -> Self {
        self.mandatory = mandatory;
        self
    }

    pub fn root(&self) -> String {
        self.path().root().to_owned()
    }

    pub fn path(&self) -> Path {
        Path { path: &self.path }
    }

    pub fn depth(&self) -> usize {
        self.path.chars().filter(|ch| *ch == '.').count()
    }

    fn name_from_path(path: &String) -> String {
        path.replace(".", "_")
    }
}

#[derive(Eq, Debug)]
pub struct Path<'a> {
    path: &'a str,
}

impl<'a> Path<'a> {
    pub fn depth(&'a self) -> usize {
        self.path
            .as_bytes()
            .iter()
            .filter(|ch| **ch == b'.')
            .count()
    }

    pub fn root(&self) -> &'a str {
        let path_bytes = self.path.as_bytes();
        for (i, &character) in path_bytes.iter().enumerate() {
            if character == b'.' {
                return &self.path[..i];
            }
        }

        self.path
    }

    pub fn as_vec(&self) -> Vec<String> {
        self.path.split('.').map(|n| n.to_owned()).collect()
    }

    pub fn split(&self) -> (&'a str, Path<'a>) {
        let path_bytes = self.path.as_bytes();
        for (i, &character) in path_bytes.iter().enumerate() {
            if character == b'.' {
                return (
                    &self.path[..i],
                    Path {
                        path: &self.path[i + 1..],
                    },
                );
            }
        }
        (
            &self.path,
            Path {
                path: &self.path[self.path.len()..],
            },
        )
    }

    pub fn as_string(&self) -> String {
        self.path.to_owned()
    }
}

impl<'a> Ord for Path<'a> {
    fn cmp(&self, other: &Path) -> Ordering {
        let self_depth = self.depth();
        let other_depth = other.depth();
        if self_depth == other_depth {
            self.path.cmp(other.path)
        } else {
            self_depth.cmp(&other_depth)
        }
    }
}

impl<'a> PartialOrd for Path<'a> {
    fn partial_cmp(&self, other: &Path) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> PartialEq for Path<'a> {
    fn eq(&self, other: &Path) -> bool {
        self.depth() == other.depth() && self.path == other.path
    }
}

#[derive(Debug, Clone, PartialEq, Hash, NamedType)]
pub struct Schema {
    fields: Vec<JsonField>,
}

impl Schema {
    pub fn new() -> Self {
        Self { fields: Vec::new() }
    }

    pub fn push(&mut self, element: JsonField) {
        self.fields.push(element)
    }

    pub fn max_depth(&self) -> usize {
        self.fields
            .iter()
            .fold(0, |acc, ref f| acc.max(f.path.matches(".").count() + 1))
    }

    pub fn len(&self) -> usize {
        self.fields.len()
    }

    pub fn iter(&self) -> Iter<JsonField> {
        self.fields.iter()
    }
}

impl IntoIterator for Schema {
    type Item = JsonField;
    type IntoIter = ::std::vec::IntoIter<JsonField>;

    fn into_iter(self) -> Self::IntoIter {
        self.fields.into_iter()
    }
}

#[derive(Debug, NamedType, PartialEq)]
pub enum Column {
    F64(Vec<f64>),
    U64(Vec<u64>),
    I64(Vec<i64>),
    F32(Vec<f32>),
    U32(Vec<u32>),
    I32(Vec<i32>),
    U96(Vec<Int96>),
    I96(Vec<Int96>),
    Bool(Vec<bool>),
    Str(Vec<String>),
    OptionF64(Vec<Option<f64>>),
    OptionU64(Vec<Option<u64>>),
    OptionI64(Vec<Option<i64>>),
    OptionF32(Vec<Option<f32>>),
    OptionU32(Vec<Option<u32>>),
    OptionI32(Vec<Option<i32>>),
    OptionU96(Vec<Option<Int96>>),
    OptionI96(Vec<Option<Int96>>),
    OptionBool(Vec<Option<bool>>),
    OptionStr(Vec<Option<String>>),
}

pub trait DisplayEnumType {
    fn display_type(&self) -> &'static str;
}

impl DisplayEnumType for Column {
    fn display_type(&self) -> &'static str {
        match *self {
            Column::F64(_) => "Column::F64",
            Column::U64(_) => "Column::U64",
            Column::I64(_) => "Column::I64",
            Column::Bool(_) => "Column::Bool",
            Column::Str(_) => "Column::Str",
            Column::F32(_) => "Column::F32",
            Column::U32(_) => "Column::U32",
            Column::I32(_) => "Column::I32",
            Column::U96(_) => "Column::U96",
            Column::I96(_) => "Column::I96",
            Column::OptionF64(_) => "Column::OptionF64",
            Column::OptionU64(_) => "Column::OptionU64",
            Column::OptionBool(_) => "Column::OptionBool",
            Column::OptionStr(_) => "Column::OptionStr",
            Column::OptionI64(_) => "Column::OptionI64",
            Column::OptionF32(_) => "Column::OptionF32",
            Column::OptionU32(_) => "Column::OptionU32",
            Column::OptionI32(_) => "Column::OptionI32",
            Column::OptionU96(_) => "Column::OptionU96",
            Column::OptionI96(_) => "Column::OptionI96",
        }
    }
}

impl Column {
    pub fn push_bool(&mut self, value: Option<bool>) -> Result<(), String> {
        match self {
            Column::Bool(ref mut vals) => {
                if let Some(v) = value {
                    vals.push(v);
                    Ok(())
                } else {
                    Err(format!("Can not store `None` in `{}`", self.display_type()))
                }
            }
            Column::OptionBool(ref mut vals) => {
                vals.push(value);
                Ok(())
            }
            col @ _ => Err(format!(
                "Can not store `Option<bool>` in `{}`",
                col.display_type()
            )),
        }
    }

    pub fn push_int(&mut self, value: Option<i64>) -> Result<(), String> {
        match self {
            Column::I64(ref mut vals) => {
                if let Some(v) = value {
                    vals.push(v);
                    Ok(())
                } else {
                    Err(format!("Can not store `None` in `{}`", self.display_type()))
                }
            }
            Column::OptionI64(ref mut vals) => {
                vals.push(value);
                Ok(())
            }
            col @ _ => Err(format!(
                "Can not store `Option<i64>` in `{}`",
                col.display_type()
            )),
        }
    }

    pub fn push_uint(&mut self, value: Option<u64>) -> Result<(), String> {
        match self {
            Column::U64(ref mut vals) => {
                if let Some(v) = value {
                    vals.push(v);
                    Ok(())
                } else {
                    Err(format!("Can not store `None` in `{}`", self.display_type()))
                }
            }
            Column::OptionU64(ref mut vals) => {
                vals.push(value);
                Ok(())
            }
            col @ _ => Err(format!(
                "Can not store `Option<u64>` in `{}`",
                col.display_type()
            )),
        }
    }

    pub fn push_float(&mut self, value: Option<f64>) -> Result<(), String> {
        match self {
            Column::F64(ref mut vals) => {
                if let Some(v) = value {
                    vals.push(v);
                    Ok(())
                } else {
                    Err(format!("Can not store `None` in `{}`", self.display_type()))
                }
            }
            Column::OptionF64(ref mut vals) => {
                vals.push(value);
                Ok(())
            }
            col @ _ => Err(format!(
                "Can not store `Option<f64>` in `{}`",
                col.display_type()
            )),
        }
    }

    pub fn push_str(&mut self, value: Option<String>) -> Result<(), String> {
        match self {
            Column::Str(ref mut vals) => {
                if let Some(v) = value {
                    vals.push(v);
                    Ok(())
                } else {
                    Err(format!("Can not store `None` in `{}`", self.display_type()))
                }
            }
            Column::OptionStr(ref mut vals) => {
                vals.push(value);
                Ok(())
            }
            col @ _ => Err(format!(
                "Can not store `Option<String>` in `{}`",
                col.display_type()
            )),
        }
    }
}

pub struct Table {
    cols: HashMap<String, Column>,
    names: Vec<String>,
    schema: Schema,
}

pub trait Push<T> {
    fn push_value(&mut self, name: &String, value: Option<T>) -> Result<(), String>;
}

impl Push<bool> for Table {
    fn push_value(&mut self, name: &String, value: Option<bool>) -> Result<(), String> {
        match self.cols.get_mut(name) {
            Some(ref mut col) => col.push_bool(value),
            None => Err(format!("Column `{}` not found in table", name)),
        }
    }
}

impl Push<u64> for Table {
    fn push_value(&mut self, name: &String, value: Option<u64>) -> Result<(), String> {
        match self.cols.get_mut(name) {
            Some(ref mut col) => col.push_uint(value),
            None => Err(format!("Column `{}` not found in table", name)),
        }
    }
}

impl Push<i64> for Table {
    fn push_value(&mut self, name: &String, value: Option<i64>) -> Result<(), String> {
        match self.cols.get_mut(name) {
            Some(ref mut col) => col.push_int(value),
            None => Err(format!("Column `{}` not found in table", name)),
        }
    }
}

impl Push<f64> for Table {
    fn push_value(&mut self, name: &String, value: Option<f64>) -> Result<(), String> {
        match self.cols.get_mut(name) {
            Some(ref mut col) => col.push_float(value),
            None => Err(format!("Column `{}` not found in table", name)),
        }
    }
}

impl Push<String> for Table {
    fn push_value(&mut self, name: &String, value: Option<String>) -> Result<(), String> {
        match self.cols.get_mut(name) {
            Some(ref mut col) => col.push_str(value),
            None => Err(format!("Column `{}` not found in table", name)),
        }
    }
}

impl Table {
    pub fn new(schema: &Schema) -> Self {
        let mut table = Self {
            cols: HashMap::with_capacity(schema.fields.len()),
            names: Vec::with_capacity(schema.fields.len()),
            schema: schema.clone(),
        };
        for ref item in &schema.fields {
            table.names.push(item.path.clone());
            table.cols.insert(
                item.path.clone(),
                column_from_type(&item.type_, item.mandatory).unwrap(),
            );
        }
        table
    }

    pub fn columns(&self) -> Vec<&Column> {
        self.names.iter().map(|n| self.cols.get(n).unwrap()).collect()
    }
}

pub fn column_from_type(t: &Type, mandatory: bool) -> Result<Column, String> {
    if mandatory {
        match t {
            Type::Bool => Ok(Column::Bool(Vec::new())),
            Type::Float => Ok(Column::F64(Vec::new())),
            Type::Int => Ok(Column::I64(Vec::new())),
            Type::Str => Ok(Column::Str(Vec::new())),
            _ => Err(format!(
                "Unsupported type `{:?}`, mandatory: `{:?}`",
                t, mandatory
            )),
        }
    } else {
        match t {
            Type::Bool => Ok(Column::OptionBool(Vec::new())),
            Type::Float => Ok(Column::OptionF64(Vec::new())),
            Type::Int => Ok(Column::OptionI64(Vec::new())),
            Type::Str => Ok(Column::OptionStr(Vec::new())),
            _ => Err(format!(
                "Unsupported type `{:?}`, mandatory: `{:?}`",
                t, mandatory
            )),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::Number;
    use serde_json::de::ParserNumber;

    fn schema() -> Schema {
        let mut schema = Schema::new();
        schema.push(
            JsonField::new("id".to_owned())
                .set_type(Type::Int)
                .set_mandatory(true),
        );
        schema.push(
            JsonField::new("foo".to_owned())
                .set_type(Type::Float)
                .set_mandatory(true),
        );
        schema.push(
            JsonField::new("bar".to_owned())
                .set_type(Type::Str)
                .set_mandatory(false),
        );
        schema
    }

    #[test]
    fn test_Table_new() {
        let schema = schema();
        let table = Table::new(&schema);

        assert_eq!(
            &vec!["id".to_owned(), "foo".to_owned(), "bar".to_owned()],
            &table.names
        );
        assert_eq!(
            vec![
                &Column::I64(Vec::new()),
                &Column::F64(Vec::new()),
                &Column::OptionStr(Vec::new())
            ],
            table.columns()
        );
    }

    #[test]
    fn test_Schema_max_len() {
        let mut s = Schema::new();
        s.push(JsonField::new("foo.bar".to_owned()));
        s.push(JsonField::new("foo".to_owned()));
        s.push(JsonField::new("foo.bar.baz".to_owned()));
        s.push(JsonField::new("qux.quux".to_owned()));

        assert_eq!(3, s.max_depth())
    }

    #[test]
    fn test_Schema_len() {
        let mut s = Schema::new();
        s.push(JsonField::new("foo.bar".to_owned()));
        s.push(JsonField::new("foo".to_owned()));
        s.push(JsonField::new("foo.bar.baz".to_owned()));
        s.push(JsonField::new("qux.quux".to_owned()));

        assert_eq!(4, s.len())
    }

    #[test]
    fn test_Path_root() {
        let path_string = "foo.bar.baz";
        let path_str_2 = "baz";
        let path1 = Path { path: path_string };
        let path2 = Path { path: path_str_2 };
        assert_eq!(path1.root(), "foo");
        assert_eq!(path2.root(), "baz")
    }

    #[test]
    fn test_Path_split() {
        let path_string = "foo.bar.baz";
        let path_str_2 = "baz";
        let path1 = Path { path: path_string };
        let path2 = Path { path: path_str_2 };
        assert_eq!(path1.split(), ("foo", Path { path: "bar.baz" }));
        assert_eq!(path2.split(), ("baz", Path { path: "" }))
    }

    #[test]
    fn test_Path_depth() {
        let path = Path { path: "foo" };
        let path2 = Path { path: "foo.bar" };

        assert_eq!(path.depth(), 0);
        assert_eq!(path2.depth(), 1);
    }

    #[test]
    fn test_Path_ordering() {
        let mut paths = vec![
            Path {
                path: "quux.baz.bar",
            },
            Path { path: "foo" },
            Path { path: "bar" },
            Path { path: "quux.foo" },
            Path {
                path: "quux.bar.foo",
            },
            Path { path: "qux.quux" },
            Path {
                path: "baz.bar.foo",
            },
        ];

        paths.sort();

        let expected = vec![
            Path { path: "bar" },
            Path { path: "foo" },
            Path { path: "quux.foo" },
            Path { path: "qux.quux" },
            Path {
                path: "baz.bar.foo",
            },
            Path {
                path: "quux.bar.foo",
            },
            Path {
                path: "quux.baz.bar",
            },
        ];
        assert_eq!(expected, paths)
    }
}
